#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .settings import INSTALLED_APPS

INSTALLED_APPS.append('django_extensions',)

# CORS
CORS_ORIGIN_ALLOW_ALL = False
CORS_ORIGIN_WHITELIST = (
    u"http://localhost:8080",
    u"http://127.0.0.1:8080"
)
