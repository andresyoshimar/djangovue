const BundleTracker = require('webpack-bundle-tracker')

module.exports = {
  // baseUrl: process.env.NODE_ENV === 'production' ? '/static/' : 'http://localhost:8080/',
  publicPath: process.env.NODE_ENV === 'production' ? '/static/' : 'http://localhost:8080/',
  outputDir: './dist/',

  chainWebpack: process.env.NODE_ENV !== 'production' ? () => {} : config => {
    config.optimization
      .splitChunks(false)
    config
      .plugin('BundleTracker')
      .use(BundleTracker, [{ filename: 'webpack-stats.json' }])

    config.resolve.alias
      .set('__STATIC__', 'static')

    config.devServer
      .public('/')
      .hotOnly(true)
      .watchOptions({ poll: 1000 })
      .https(false)
      .headers({ 'Access-Control-Allow-Origin': ['*'] })
  }
}
